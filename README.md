# Slack Full Channel History

A simple python script for getting the full message history of a single slack channel.

## Features

- Supports converting all userId's to the real name, with a fallback to their user name
- Will replace any usernames found in the messages with that user's real name
- Supports getting a channel by channel name or id
- Will grab all UTF-8 compatible characters
- Some light error handling/exception management
- Does not pull in attachments, gifs, or other non-text messages

## Getting Started

Make sure slackclient and json packages are installed:
```
pip install slackclient
pip install json
```

First, edit the config.json

```
{
    "slackToken": "YourSlackAppTokenHere",
    "slackChannelId": "optional-if-known",
    "slackChannelName": "channel-name-here-if-id-is-unknown"
}
```
You're required to put in the slackToken and at least fill out either the slackChannelId or the slackChannelName. If you know the ID of the channel, use that. If you don't, the script will page through and find the id for you as long as you provide a valid name.

Then just run:
```
python importer.py
```
A _messages.txt_ file will appear if successful. This should have all of the messages in order from oldest to newest.

### Prerequisites

- Python 3.3+
- slackclient
- json

## Contributing

Just shoot me a pull request if you find any issues or bugs or want to extend this further.

## Authors

* **Hudson Akridge** [Drop me an email if you'd like](mailto:hudson.akridge@gmail.com)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details