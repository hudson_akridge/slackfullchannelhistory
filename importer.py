from datetime import datetime
import json
from slackclient import SlackClient

class SlackMessage(object):
    def __init__(self, message, user, time):
        self.message = message
        self.user = user
        self.time = time


UNKNOWN_USER_NAME = "unknown"

def main():
    config = json.load(open("Config.json", "r"))
    slack_client = get_client(config)
    userid_realname_lookup = {}
    file_pointer = open("messages.txt", "w", encoding="utf-8")
    try:
        write_channel_history_to_file(config, slack_client, userid_realname_lookup, file_pointer)
    finally:
        file_pointer.close()

def add_user_info(existing_users_dict, user_ids, slack_client):
    new_user_ids = filter(lambda x:
                          x is not UNKNOWN_USER_NAME
                          and x not in existing_users_dict, user_ids)
    for user_id in new_user_ids:
        user_info = slack_client.api_call("users.info", user=user_id)
        user = user_info["user"]
        existing_users_dict[user_id] = user["real_name"] if "real_name" in user else user["name"]

def get_client(config):
    slack_token = config["slackToken"]
    return SlackClient(slack_token)

def multiple_replace(text, word_dict):
    for key in word_dict:
        text = text.replace(key, word_dict[key])
    return text

def get_timestamp_from_message(msg):
    return datetime.fromtimestamp(float(msg["ts"]))

def write_messages(file_pointer, messages, oldest_ts, userid_realname_lookup):
    for msg in messages:
        #we only care about messages with a user attached to them.  Things
        #without users are attachments, and other crap we don't care about
        if "user" not in msg:
            continue

        current_timestamp = get_timestamp_from_message(msg)
        username = userid_realname_lookup[msg["user"]]
         #get rid of multiple returns from slack, as well as changing all the userid aliases to real users
        message_text = multiple_replace(str(msg["text"]), userid_realname_lookup).replace("\n\n", "\n")
        slack_message = SlackMessage(message_text, username, current_timestamp)
        line_to_write = "{0.user} ({0.time}): {0.message}\n".format(slack_message)
        file_pointer.write(line_to_write)

def get_next_oldest_timestamp(latest_message, oldest_ts):
    latest_message_ts = get_timestamp_from_message(latest_message)
    return latest_message_ts if oldest_ts < latest_message_ts else oldest_ts

def get_channel_id(config, slack_client):
    channel_id = config.get("slackChannelId","")
    if channel_id != "":
        return channel_id

    channel_name = config.get("slackChannelName","")
    if channel_name == "":
        raise KeyError("slackChannelName or slackChannelId must be set to a valid value in the config to continue.")
    
    next_cursor = ""
    while True:
        channel_list_response = slack_client.api_call("channels.list", limit=1000, exclude_members=True, cursor=next_cursor)
        if "channels" not in channel_list_response:
            break

        matching_channels = [channel for channel in channel_list_response["channels"] if str(channel["name"]).lower == str(channel_name).lower]
        if matching_channels:
            return matching_channels[0]["id"]

        metadata = channel_list_response["response_metadata"]
        if "next_cursor" not in metadata:
            break
        
        next_cursor = str(metadata["next_cursor"])

    raise ValueError("Channel {0} was not found in the list of channels.".format(channel_name))
    

def write_channel_history_to_file(config, slack_client, userid_realname_lookup, file_pointer):
    slack_channel_id = get_channel_id(config, slack_client)
    current_oldest_ts = datetime(2000,1,1) #funfact, datetime.min fails when calling .timestamp(), needs to be 1979 or newer.
    has_more = True
    while has_more:
        oldest_ts_queryparameter = current_oldest_ts.timestamp()
        channel_history_response = slack_client.api_call("channels.history", channel=slack_channel_id, count=200, oldest=oldest_ts_queryparameter)
        has_more = channel_history_response["has_more"] == True
        messages = channel_history_response["messages"]
        current_oldest_ts = get_next_oldest_timestamp(messages[0], current_oldest_ts)
        
        #perform user lookups
        users = set([msg.get("user", UNKNOWN_USER_NAME) for msg in messages])
        add_user_info(userid_realname_lookup, users, slack_client)
        
        reversed_list = messages[::-1] #slack gives us the first ordinal message back as the newest
        write_messages(file_pointer, reversed_list, current_oldest_ts, userid_realname_lookup)

main()